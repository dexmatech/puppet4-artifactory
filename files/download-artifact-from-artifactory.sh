#!/bin/bash

# downloads latest version of an artifact from artifactory

set -e

usage(){
  echo "Usage: $*" >&2
  exit 64
}

repo=""
group=""
artifact=""
classifier=""
username=""
password=""
output=""

while getopts r:g:a:c:u:p:o:v:e: OPT; do
  case "${OPT}" in
    r) repo="${OPTARG}";;
    g) group="${OPTARG}";;
    a) artifact="${OPTARG}";;
    c) classifier="${OPTARG}";;
    u) username="${OPTARG}";;
    p) password="${OPTARG}";;
    o) output="${OPTARG}";;
    v) version="${OPTARG}";;
    e) extension="${OPTARG}";;
  esac
done
shift $(( $OPTIND - 1 ))

if [ -z "${repo}" ] || [ -z "${group}" ] || [ -z "${artifact}" ] || [ -z "${username}" ] || [ -z "${password}" ] ; then
  usage "-r REPOSITORY -g GROUPID -a ARTIFACTID -u USERNAME -p PASSWORD [-c CLASSIFIER]"
fi

auth=
if [[ "${username}" != "" ]]  && [[ "${password}" != "" ]]
then
    auth="-u $username:$password"
fi
# Maven artifact location
ga=${group//./\/}/$artifact
repopath=$repo/list/virtual-all/$ga

##XXX IF/ELSE for snapshot
if [[ $version = *"SNAPSHOT"* ]]; then
  build=`curl -s $auth $repopath/$version/maven-metadata.xml | grep '<lastUpdated>' | head -1 | sed "s/.*<lastUpdated>\([^<]*\)<\/lastUpdated>.*/\1/"`
  if [ -z "${build}" ]; then
    file=$artifact-$build.$extension
  else
    #file=$artifact-$build-$classifier.$extension
    file=$artifact-$version.$extension
  fi
else
  file=$artifact-$version.$extension
fi

url=$repopath/$version/$file

# Download
# echo $url
curl -s $auth $url -o $output
